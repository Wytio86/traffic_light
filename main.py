import pytest
import logging


logging.basicConfig(level=logging.DEBUG, filename="logfile",
                    format="%(asctime)s:%(levelname)s:%(message)s",
                    datefmt="%Y-%m-%d %H:%M:%S")


@pytest.fixture()
def fix_file():
    """
    Description:
        Function reads a file with date and creates a list of binary integers
    Returns:
        list of binary integers
    """
    arr = []
    path = "/Users/vitalijusivanovas/Desktop/data.txt"  # file with stored data
    with open(path) as file:
        for x in file:
            arr.append(int((x.replace(",", "").rstrip("\n")), 2))
    return arr


def check_if_correct(arr):
    """
    Description:
        Check if traffic light is working correctly.
    Parameters:
        arr (list): list of binary integers
    Returns:
        return true if everything is correct

    """
    color_dict = {0: 'blank', 1: "greenTurn", 2: "green", 4: "yellow", 8: "red"}
    total_photo = len(arr)
    current_light = color_dict[arr[0]]
    previous_light = color_dict[arr[0]]

    for x, z in enumerate(arr):
        if total_photo - 1 == x:
            break
        else:
            if color_dict[z] != current_light:
                previous_light = current_light  # save a previous traffic light color
                current_light = color_dict[z]  # save a current traffic light color

            if color_dict[z] == "green":
                if color_dict[arr[x + 1]] == "green" or color_dict[arr[x + 1]] == "blank"\
                        or color_dict[arr[x + 1]] == "yellow":
                    logging.debug(f"continue, current light is {color_dict[z]}")
                    continue
                else:
                    logging.error(f"current light is {current_light}, next cannot be {color_dict[arr[x+1]]}")
                    raise Exception("Error")
            elif color_dict[z] == "blank":
                if color_dict[arr[x + 1]] == "green" or color_dict[arr[x + 1]] == "blank":
                    logging.debug(f"continue, current color is {color_dict[z]}")
                    continue
                else:
                    logging.error(f"current light is {current_light}, next cannot be {color_dict[arr[x + 1]]}")
                    raise Exception("Error")
            elif color_dict[z] == "yellow":
                if previous_light == "green":
                    if color_dict[arr[x + 1]] == "red" or color_dict[arr[x + 1]] == "yellow":
                        logging.debug(f"continue, current light is {color_dict[z]}")
                        continue
                    else:
                        logging.error(f"current light is {current_light}, previous was {previous_light},"
                                      f"next cannot be {color_dict[arr[x + 1]]}")
                        raise Exception("Error")
                elif previous_light == "red":
                    if color_dict[arr[x + 1]] == "greenTurn" or color_dict[arr[x + 1]] == "yellow"\
                            or color_dict[arr[x + 1]] == "green":
                        logging.debug(f"continue, current light is {color_dict[z]}")
                        continue
                    else:
                        logging.error(f"current light is {current_light}, previous was {previous_light},"
                                      f"next cannot be {color_dict[arr[x + 1]]}")
                        raise Exception("Error")
            elif color_dict[z] == "red":
                if color_dict[arr[x + 1]] == "yellow" or color_dict[arr[x + 1]] == "red":
                    logging.debug(f"continue, current light is {color_dict[z]}")
                    continue
                else:
                    logging.error(f"current light is {current_light}, next cannot be {color_dict[arr[x + 1]]}")
                    raise Exception("Error")
            elif color_dict[z] == "greenTurn":
                if color_dict[arr[x + 1]] == "green" or color_dict[arr[x + 1]] == "greenTurn":
                    logging.debug(f"continue, current light is {color_dict[z]}")
                    continue
                else:
                    logging.error(f"current light is {current_light}, next cannot be {color_dict[arr[x + 1]]}")
                    raise Exception("Error")
    logging.info("Traffic light passed a testing")
    return True


def test_check_if_correct(fix_file):
    assert check_if_correct(fix_file)


# check_if_correct(fix_file())
